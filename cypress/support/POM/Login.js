import Data from '../../fixtures/data.json'

class Login_Po{
    Login(){
        cy.visit(Data.URL)   
        cy.login(Data.email,Data.password) // we created a Cypress custom command "login" in Support/Command.js
    }
}
export default Login_Po;